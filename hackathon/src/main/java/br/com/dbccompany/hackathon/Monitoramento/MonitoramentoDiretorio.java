package br.com.dbccompany.hackathon.hackathon.Monitoramento;

import br.com.dbccompany.hackathon.hackathon.URI.URI;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@Component
public class MonitoramentoDiretorio {

    //INSERIR LOGS

    @PostConstruct
    @Async
    public static void run(){
        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            Path diretorio = Paths.get(URI.INPUT);
            diretorio.register(watcher, ENTRY_CREATE);

            ChecaExtensaoArquivo extensaoArquivo = new ChecaExtensaoArquivo();

            System.out.println("Sistema de monitoramento de diretorio iniciado em: "
                    + URI.INPUT);

            while (true) {
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException ex) {
                    return;
                }

                for (WatchEvent<?> eventosNoDiretorio : key.pollEvents()) {
                    WatchEvent.Kind<?> tipoDeEvento = eventosNoDiretorio.kind();
                    WatchEvent<Path> evento = (WatchEvent<Path>) eventosNoDiretorio;
                    if (tipoDeEvento.equals(ENTRY_CREATE)) {
                        if (extensaoArquivo.isArquivoDat(evento.context())) {
                            //LOG DE ENTRADA
                            //CHAMAR LOGICA DO ISAIAS
                            System.out.println("...ENTROU ARQUIVO DAT E PODE SER PROCESSADO");
                        } else {
                            //LOG DE ARQUIVO NAO .DAT INSERIDO
                            System.out.println(".. ENTROU ARQUIVO NAO DAT E NAO PODE SER PROCESSADO");
                        }
                    }
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }

        } catch (IOException ex) {
            //LOG DE EXCECAO
            System.err.println(ex.getMessage());
        }
    }
}
