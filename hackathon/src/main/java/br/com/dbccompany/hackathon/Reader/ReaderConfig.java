package br.com.dbccompany.hackathon.Reader;

import br.com.dbccompany.hackathon.Model.ClienteModel;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.validation.BindException;

@Configuration
public class ReaderConfig {

    @Bean
    public FlatFileItemReader<ClienteModel> arquivoPessoaReader() {
        return new FlatFileItemReaderBuilder<ClienteModel>()
                .name("ClienteModel")
                .resource(new FileSystemResource("data/in/flatFile.dat"))
                .delimited()
                .names("id", "cnpj", "nome", "areaAtuacao")
                .fieldSetMapper(fieldSetMapper())
                .build();
    }

    private FieldSetMapper<ClienteModel> fieldSetMapper() {
        return new FieldSetMapper<ClienteModel>() {

            @Override
            public ClienteModel mapFieldSet(FieldSet fieldSet) throws BindException {
                ClienteModel clienteModel = new ClienteModel();

                clienteModel.setId(fieldSet.readInt("id"));
                clienteModel.setNome(fieldSet.readString("cnpj"));
                clienteModel.setNome(fieldSet.readString("nome"));
                clienteModel.setAreaAtuacao(fieldSet.readString("areaAtuacao"));

                return clienteModel;
            }
        };
    }
}
